package com.jaminjake.hibernate;

/**
 * ModifyTable.java
 * Course: CIT360
 * Name: Jake Evans
 * Week: 12 Project
 */

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import java.util.*;

public class ModifyTable {

//    private String movieName;
  //  private String
//    static Movie movieObj;
//    static Session sessionObj;
//    static SessionFactory sessionFactoryObj;
    //static void addTable(){





    SessionFactory factory = null;
    Session session = null;

    private static ModifyTable single_instance = null;

    public ModifyTable() {
        factory = SessionFactoryUtils.getSessionFactory();
    }

    /**
     * Single Instance
     */
    public static ModifyTable getInstance() {
        if (single_instance == null) {
            single_instance = new ModifyTable();
        }

        return single_instance;
    }

    public Movie addMovie(String movieName, String movieGenre, String movieRating) {

        try {
            //ModifyTable mt = new ModifyTable();
            System.out.println(movieName + movieGenre + movieRating);
            session = factory.openSession();
            session.beginTransaction().begin();
            //movieName = addMovie(setMovieName);
            Movie mv = new Movie();
            //addMovie(movieName,movieGenre,movieRating);
            //this.addMovie().
            mv.setMovieName(movieName);
            mv.setMovieGenre(movieGenre);
            mv.setMovieRating(movieRating);

            //Movie movie = (Movie) session.save(mv);
            //Movie movie = new Movie();

            // Save the Movie in the database
            session.save(mv);
            //Commit the transaction
            session.getTransaction().commit();
            //SessionFactoryUtils.shutdown();
            return null;
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    }


