package com.jaminjake.hibernate;

/**
 * TestDAO.java
 * Course: CIT360
 * Name: Jake Evans
 * Week: 12 Project
 */

import org.hibernate.HibernateException;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        //Test Database Connection and open session
        TestDAO t = TestDAO.getInstance();


        System.out.println("List Database Table = movie :\n");

        //List Database Entries
        List<Movie> m = t.getMovies();
        for (Movie i : m) {
            //List Database table movie
            System.out.println(i);
        }

        System.out.println("\nList 1st entry in Database Table = movie :\n");

        //List First Entry in Database table movie
        System.out.println(t.getMovie(1));

        //Add table to database
        //Movie mv = new Movie();
        String movieName = "A Little Princess";
        String movieGenre = "Drama";
        String movieRating = "G";
        //mv.setMovieName(movieName);
        //mv.setMovieGenre(movieGenre);
        //mv.setMovieRating(movieRating);
        //TestDAO t2 = TestDAO.getInstance();
        t.addMovie(movieName,movieGenre,movieRating);

        System.out.println("List Database Table = movie :\n");

        //List Database Entries
        List<Movie> m1 = t.getMovies();
        for (Movie i : m1) {
            //List Database table movie
            System.out.println(i);
        }


    }


}


